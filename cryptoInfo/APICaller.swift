//
//  APICaller.swift
//  CryptoInformer
//
//  Created by Maksim on 14.07.2022.
//

import Foundation

final class APICaller {
    static let shared = APICaller()
    
    private struct Constants {
        static let apiKey = "7aa07036dbfefae647b0513134c99cf3f6761fd6"
        static let assetsEndpoint = "https://api.nomics.com/v1/currencies/"
    }
    
    private init() {
        
    }
    
    public func getAllCryptoData(
        complition: @escaping (Result<[Crypto], Error>) -> Void
    ) {
        guard let url = URL(string: Constants.assetsEndpoint + "ticker?key=" + Constants.apiKey + "&ranks=1&interval=1d,30d&convert=USD&per-page=15&page=1")  else {
            return
        }
        let task = URLSession.shared.dataTask(with: url) { data, _, error in
            guard let data = data, error == nil else {
                return
            }
            
            do {
                // MARK: - Decode process
                let jsonResult = try JSONDecoder().decode([Crypto].self, from: data)
                complition(.success(jsonResult))
            }
            catch {
                complition(.failure(error))
            }
        }
        task.resume()
    }
    
    
    
}
