//
//  TableViewCellViewModel.swift
//  CryptoInformer
//
//  Created by Maksim on 21.07.2022.
//

import Foundation

struct TableViewCellViewModel {
    let name: String
    let symbol: String
    let price: String
}



