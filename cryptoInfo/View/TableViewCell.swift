//
//  TableViewCell.swift
//  cryptoInfo
//
//  Created by Maksim on 05.08.2022.
//

import UIKit

class TableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var symbolLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var logoImageView: UIImageView!
    
    static let identifier = "CryptoTableViewCell"
    
    func configure(with viewModel: TableViewCellViewModel) {
        nameLabel.text = viewModel.name
        symbolLabel.text = viewModel.symbol
        priceLabel.text = viewModel.price
        
        let url = URL(string: "https://cryptoicons.org/api/icon/\(viewModel.symbol.lowercased())/50)")
        DispatchQueue.main.async {
            if let data = try? Data(contentsOf: url!) {
                self.logoImageView.image = UIImage(data: data)
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
