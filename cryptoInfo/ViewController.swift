//
//  ViewController.swift
//  cryptoInfo
//
//  Created by Maksim on 05.08.2022.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    public var viewModels = [TableViewCellViewModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.register(UINib(nibName: String(describing: TableViewCell.self), bundle: nil), forCellReuseIdentifier: TableViewCell.identifier)
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        APICaller.shared.getAllCryptoData { [weak self] result in
                    switch result {
                    case .success(let models):
                        self?.viewModels = models.compactMap({
                            TableViewCellViewModel(
                            name: $0.name ?? "",
                            symbol: $0.symbol ?? "",
                            price: $0.price ?? "")
                        })
                   
                        DispatchQueue.main.async {
                            self?.tableView.reloadData()
                        }
                    case .failure(_):
                        print("OOPS! Not now, boy!")
                    }
            }
    }


}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCell.identifier, for: indexPath) as? TableViewCell else {
            return UITableViewCell()            // переделать в return empty cell
        }
        cell.configure(with: viewModels[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
    
    

