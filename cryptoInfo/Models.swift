//
//  Models.swift
//  CryptoInformer
//
//  Created by Maksim on 15.07.2022.
//

import Foundation

struct Crypto: Codable {
    let id: String?
    let currency: String?
    let symbol: String?
    let name: String?
    let price: String?
}
